# Challenge fullstack

Le but de ce challenge est de dynamiser un site statique, pour cela, plusieurs choix sont possibles, vous pouvez créer une API dans le langage de votre choix (Java, Golang, Python, etc..), faire des requêtes en Javascript vanilla ou bien utiliser un framework frontend pour dynamiser tout ça et éviter d'avoir aussi plusieurs pages HTML qui utilisent le même code (par exemple le menu, le footer, etc..). Ou bien vous pouvez utiliser PHP ou tout autre langage qui n'a pas besoin de créer une API pour intéragir avec une base de données.

## Template HTML à utiliser

Le code source du template HTML à utiliser est disponible [ici](https://gitlab.com/alexandrebarde/dellifood).

Le site propose des recettes de cuisine, différents onglets sont disponibles afin de naviguer entre les différents types de recettes.
Seulement l'administrateur peut se connecter au site via le bouton connexion.
L'administrateur peut effectuer les tâches suivantes :
- Ajouter une recette
- Supprimer une recette
- Editer une recette
- Voir les statistiques

La partie administrateur n'a pas de design, vous pouvez faire ce que vous voulez. Le plus intéressant est le backend, le CSS ne sera pas noté.

## Fonctionnalités à implémenter

### Débutant

#### Utilisateur

- [ ] Voir les recettes dans les différents onglets
- [ ] Voir les détails d'une recette

### Intermédiaire

#### Utilisateur

- [ ] La barre de recherche doit être fonctionnelle

#### Administration

- [ ] Connexion
- [ ] Ajouter une recette
- [ ] Supprimer une recette

#### Avancé

### Administration

- [ ] Editer une recette
- [ ] Voir des statistiques sur les recettes (nombre de vues par exemple)

## Déploiement

Vous allez avoir besoin d'une base de données ainsi que d'un serveur web, vous pouvez utiliser [000webhost](https://fr.000webhost.com/) qui est gratuit pour héberger tout votre code.
Si vous avez un serveur dédié / VPS vous pouvez aussi le mettre dessus.